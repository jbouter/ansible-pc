# Ansible PC

![Ansible PC logo](/images/pc.png)

This will set up my PC to my liking. Supports both KDE and GNOME.
[Explore the Ansible docs](https://docs.ansible.com/ansible/latest/index.html)

Tested on:

* Arch Linux
* Debian Sid
* Fedora 35
* OpenSUSE Tumbleweed
* Ubuntu 22.04 (and derivatives)

## Preface/Notes

Before starting, a few things must be configured on the OS.

### Ubuntu/Debian

```bash
apt install -y git python3-virtualenv python3-venv python3-dev python3-wheel libxml2-dev libxslt-dev libssl-dev linux-headers-generic build-essential
```

### Fedora

```bash
dnf install -y git python3-virtualenv python3-devel libxml2-devel kernel-headers kernel-devel
```

### Arch Linux

```bash
sudo pacman -S git python-pip python-virtualenv base-devel
```

## Setting up the Virtual Environment

Set up the virtual environment as follows:

```bash
python3 -m venv env
source env/bin/activate
pip install --upgrade pip
pip install -r requirements.txt
```

Every time you work with Ansible, source the virtualenv (`source env/bin/activate`)

## Downloading the required ansible collections from Ansible Galaxy

Download the required Ansible collections from galaxy by running the following command

```bash
ansible-galaxy install -r requirements.yml
```

## Configuration

Username can be changed from `jbouter` to whatever you like in `inventory/host_vars/localhost/user.yml`
GRUB password must be reconfigured by yourself in `inventory/host_vars/localhost/grub.yml` by encrypting a new string:

Copy the value from the output of the following command:

```bash
grub-mkpasswd-pbkdf2
```

And then encrypt it using the ansible vault:

```bash
ansible-vault encrypt_string 'example'
```

Edit `inventory/host_vars/localhost/user.yml` and `inventory/host_vars/localhost/grub.yml` with proper data.

Please read through roles for an explanation on what it all does.

## Running the privisioning

Executing is as simple as running:

```bash
ansible-playbook playbooks/provision.yml
```

## Sources

Icons made by [Prosymbols](https://www.flaticon.com/authors/prosymbols) from [www.flaticon.com](https://www.flaticon.com/)
