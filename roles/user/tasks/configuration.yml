---

- name: Configure passwordless sudo for wireguard commands
  ansible.builtin.copy:
    dest: /etc/sudoers.d/wireguard
    owner: root
    group: root
    mode: '0440'
    content: |
      # Allow members of group sudo/wheel to execute wireguard commands
      Cmnd_Alias WIREGUARD = /usr/bin/wg show, \
                              /usr/bin/wg-quick up wg[0-9], \
                              /usr/bin/wg-quick down wg[0-9]

      %sudo ALL = NOPASSWD: WIREGUARD
      %wheel ALL = NOPASSWD: WIREGUARD

- name: Configure passwordless sudo for fwupdmgr commands
  ansible.builtin.copy:
    dest: /etc/sudoers.d/fwupd
    owner: root
    group: root
    mode: '0440'
    content: |
      # Allow members of group sudo/wheel to execute fwupdmgr commands
      Cmnd_Alias FWUPD = /usr/bin/fwupdmgr update, \
                         /usr/bin/fwupdmgr upgrade, \
                         /usr/bin/fwupdmgr refresh

      %sudo ALL = NOPASSWD: FWUPD
      %wheel ALL = NOPASSWD: FWUPD

- name: Configure passwordless sudo for flatpak commands
  ansible.builtin.copy:
    dest: /etc/sudoers.d/flatpak
    owner: root
    group: root
    mode: '0440'
    content: |
      # Allow members of group sudo/wheel to execute flatpak commands
      Cmnd_Alias FLATPAK = /usr/bin/flatpak update, \
                           /usr/bin/flatpak install *, \
                           /usr/bin/flatpak uninstall *

      %sudo ALL = NOPASSWD: FLATPAK
      %wheel ALL = NOPASSWD: FLATPAK

- name: Configure passwordless sudo for pkcon commands
  ansible.builtin.copy:
    dest: /etc/sudoers.d/pkcon
    owner: root
    group: root
    mode: '0440'
    content: |
      # Allow members of group sudo/wheel to execute pkcon commands
      Cmnd_Alias PKCON = /usr/bin/pkcon refresh, \
                         /usr/bin/pkcon update, \
                         /usr/bin/pkcon update *, \
                         /usr/bin/pkcon install *, \
                         /usr/bin/pkcon remove *, \
                         /usr/bin/pkcon offline-trigger, \
                         /usr/bin/pkcon get-updates

      %sudo ALL = NOPASSWD: PKCON
      %wheel ALL = NOPASSWD: PKCON

- name: Configure profile
  ansible.builtin.copy:
    src: profile
    dest: "{{ item.homedir }}/.profile"
    owner: "{{ item.user }}"
    group: "{{ item.user }}"
    mode: '0644'
  loop:
    - { "homedir": "/home/{{ user.username }}", "user": "{{ user.username }}" }
    - { "homedir": "/root", "user": "root" }

- name: Configure vim
  ansible.builtin.copy:
    src: vimrc
    dest: "{{ item.homedir }}/.vimrc"
    owner: "{{ item.user }}"
    group: "{{ item.user }}"
    mode: '0644'
  loop:
    - { "homedir": "/home/{{ user.username }}", "user": "{{ user.username }}" }
    - { "homedir": "/root", "user": "root" }

- name: Check if user's git config exists
  ansible.builtin.stat:
    path: "/home/{{ user.username }}/.gitconfig"
  register: stat_user_gitconfig

- name: Configure git for user
  ansible.builtin.template:
    src: gitconfig.j2
    dest: "/home/{{ user.username }}/.gitconfig"
    owner: "{{ user.username }}"
    group: "{{ user.username }}"
    mode: '0644'
  when: not stat_user_gitconfig.stat.exists

- name: Configure local directories for user
  ansible.builtin.file:
    path: "{{ item }}"
    state: directory
    owner: "{{ user.username }}"
    group: "{{ user.username }}"
    mode: '0755'
  loop:
    - "/home/{{ user.username }}/.local"
    - "/home/{{ user.username }}/.local/bin"
    - "/home/{{ user.username }}/.local/share"
    - "/home/{{ user.username }}/.local/share/neofetch"
